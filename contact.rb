class Contact
	#counter is a class method because it tells us how many contacts have been createed. Need to have the 'factory/class' count. 
	#Class variables are only available to the class. Can't access a class variable in an object method. Can't access a class method from an object.

	@@counter = 1000 #class variable
	attr_accessor :notes, :name, :id, :email

	def initialize() #why can't I put the arguments in here
		# @notes = "" **doesn't need a default value, because notes are being taken from the rolodex class
		# @name = name
		#@id = @@counter why would this be zero?
		@id = Contact.get_id #need to tell the class, to tell us the method. Class.get_id
	 	
 	end

	def to_s
		"id = #{id}\nName: #{name}\nEmail: #{email}\nNotes: #{notes}" #the name is being created from the add_a_contact_in_rolodex method, where I've intialized the Contact class
	end

	def self.get_id #class method - cannot access it from inside an object; instance method = things you can tell an object to do; class methods = things you can tell a class to do
 		@@counter += 1
		@@counter
	end
end

#self is the thing that is answering a message of being told what to do.
#in the rolodex object, when we send the message to Contact, what is self? It's a rolodex object. It's the thing that answers Rolodex. (1:20)	