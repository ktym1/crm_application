class Rolodex

	def initialize
	@contacts = []
	end

	def add_a_contact_in_rolodex(name)
		contact = Contact.new #creating an instance object of contact
		contact.name = name #taking the contact object, and assigning it to the argument of name
		@contacts << contact #adding the contact to the array of contacts
	end

	def see_all_contacts
		@contacts.each do |contact| #loop through our contacts list. we run the each loop in the rolodex class, instead of from the UI method
		puts contact #need to convert our names & notes to strings

		#puts contact.name - this would be stealing the wallet
		#puts contact.notes - this would be stealing the wallet
		end
	end

	def remove_a_contact(id) #deleting a contact could mess up the indices in the array
		result = mil	 #need to get what we find in the loop, out of our loop, so we can do something with it. This stores our variable.
		@contacts.each do |contact|
			if contact.id == id #if the contact being iterated through, matches the id being passed through the argument
				result = contact
			end
		end
		@contacts.delete(result) if result != nil #why can't this be in the loop? you don't want to be changing the array, that you are looping over. one of the conditions of an iterator, is that we don't change what we're iterating over.
	end                                           #need to add an accessor for id in contact.rb

	def edit_a_contact(id, attribute, updated_attribute)
		@contacts.each do |contact|
			if contact.id == id
				if attribute == "Name"
					contact.name = updated_attribute #can modify within an array, but cannot add or delete the actual array
				elsif attribute == "Email"
					contact.email = updated_attribute
				elsif attribute == "Notes"
					contact.notes = updated_attribute
				end
			end
		end
	end
	def add_a_contact_email_in_rolodex(id, email)
		@contacts.each do |contact|
			if contact.id == id
				contact.email = email 
			end
		end
	end

	def add_a_contact_notes_in_rolodex(id, notes)
		@contacts.each do |contact|
			if contact.id == id
				contact.notes = notes
			end
		end
	end
end
