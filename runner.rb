=begin User Stories
I want to see a customer's notes

=end

require './rolodex' #asking Ruby to read this file into memory
require './contact'

class Runner 
	def initialize
		@rolodex = Rolodex.new  #added after add_a_contact, since we reocgnize we're sending user input and need the initialize the rolodex class
								#every time we create a new Runner, the first thing it does, is initialize the rolodex	
	end
	def main_menu #creating the menu for the user
		puts "Kerry's Awesome CRM" #title of our CRM on user screen
		puts "1. Add a contact"
		puts "2. See all contacts"
		puts "3. Remove a contact"
		puts "4. Edit a contact"
		puts "5. Add a contact's email"
		puts "6. Add a contact's notes"
		puts "0. To Exit"
	end

	def add_a_contact #method lets us take input from the user, then push the information to the array
		puts "Enter Contact Name: "
		name = gets.chomp
		@rolodex.add_a_contact_in_rolodex(name) #need to take user input about adding a contact. then, send input to the rolodex. so need to create an instance of rolodex
	end
	
	def see_all_contacts 
		puts "All Contacts"
		@rolodex.see_all_contacts #telling rolodex class to show all contacts
		puts "-------"
	end

	def remove_a_contact
		puts "Enter Contact ID to remove"
		id = gets.chomp.to_i
		@rolodex.remove_a_contact(id)
	end

	def edit_a_contact
		puts "Enter Contact's ID to edit"
		id = gets.chomp.to_i
		
		puts "What should you edit? Name/Email/Notes"
		attribute = gets.chomp		

		puts "What would you like to change it to?"
		updated_attribute = gets.chomp

		puts "Confirm your change (Y/N)"
			input = gets.chomp
			if input === "Y" || input == "y"
			@rolodex.edit_a_contact(id, attribute, updated_attribute)			
			else input == "N" || input == "n"
				return "" #if I put "main_menu, then you'll get 2x main menus printed" 
			end		

	end
	
	def add_a_contact_email
		puts "Enter Contact's ID to attach Email"
		id  = gets.chomp.to_i
		puts "Enter contact's email"
		email = gets.chomp
		@rolodex.add_a_contact_email_in_rolodex(id, email) #take user input, send it to rolodex
	end

	def add_a_contact_notes
		puts "Enter Contact's ID to attach Notes"
		id = gets.chomp.to_i
		puts "Enter contact's notes"
		notes = gets.chomp
		@rolodex.add_a_contact_notes_in_rolodex(id, notes)
	end

	def run #creating a method to run the main menu; want to confirm that outside of this method, I can access main
		done = false #create an execution loop with while, without execution loop the program retain memory of input
		while !done			
			main_menu
			input = gets.chomp.to_i #want a customer's input to be an integer, not writing/string
			if input == 0 
				done = true #end of execution loop
			elsif input == 1
				add_a_contact #create a method above
			elsif input == 2
				see_all_contacts
			elsif input == 3
				remove_a_contact
			elsif input == 4
				edit_a_contact
			elsif input == 5
				add_a_contact_email
			elsif input == 6
				add_a_contact_notes
			end
		end 
	end

end

runner = Runner.new
runner.run